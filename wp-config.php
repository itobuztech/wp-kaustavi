<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-kaustavi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Passw0rd12345678#' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          ';9Nq,&25|;wi>Q%ba@29$]K.s[,^DiSAii3AX{#(lrM2oSBBjA:d-cl}G>/T|VDU' );
define( 'SECURE_AUTH_KEY',   '8}.^*]=oF_^!Zrjg]9X1czU_YTj7reFRv1-Ke^3S~:mZ),M,uVk_Y=B8-&!eTFf9' );
define( 'LOGGED_IN_KEY',     'QaYgFi+ZwfbI<5y%vRQ0I;D7=_|$g&^t!<!9ixjKo]{9d+NSE20@4hrLu*8qtliC' );
define( 'NONCE_KEY',         '.jV:]CjWF8ca~N&2@Eql&Ggo1MKky|-332+dLnR&Z?RVG,ibo^==9o7k>Oj=R|h6' );
define( 'AUTH_SALT',         'J{pBYa2&N3j3 lLg0@HEL$<x=A<AUQZmR`2(fwpuZNWeUE0(mx~lHJ4>80AIN4L,' );
define( 'SECURE_AUTH_SALT',  'Re_T7^?Bq(ttinW|MK;4dXF}`&V%DS3!o$sKV_04^6jtGCQ8qo6;I#8z&d{zXo6y' );
define( 'LOGGED_IN_SALT',    'c9LO4K@Jeit5{3++)z}^=PF}]g{twuraZ)u5$YnvZ`q}3r-Z/:l5<p)sTLF}@@~N' );
define( 'NONCE_SALT',        '9?fe10PAf~ES`_},,bGs^DV2u/A37RxKy[*qer((qr>{AiT,H+o41_^?lO)?X}@!' );
define( 'WP_CACHE_KEY_SALT', 'S$8MTEZN@m4ybR<&~g,g3,BRgLd(<$9_iqIY,Q.,MQE.q+H8%=|q_!uI2y%<F:+m' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
